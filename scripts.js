var paragraphs = document.querySelectorAll('.paragraph');

/** 
 * Funcion para cambiar color
 * param element = Nodo del Dom
*/
function cambiarColor(element) {
  element.style.color = 'black';
}

paragraphs.forEach(function (p, index) {
  // Cambiar de color dependiendo de la posicion
  if (index === 0) {
      p.classList.add('red');
    } else if (index === 2) {
      p.classList.add('blue');
    } else {
      p.classList.add('green');
  }
  p.addEventListener('mouseenter', function () { cambiarColor(this) });
});
